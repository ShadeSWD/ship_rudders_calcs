Ship_rudders_calcs
====
----
Creation of programs for the calculation of ship rudders
----
+ Just clone the project and install compulsory libs.
+ The main script is run_ship_rudders.py
```commandline
python3 run_ship_rudders.py
```
----
Libs
----
+ tkinter
+ graphics.py
+ pathlib
```commandline
pip3 install graphics.py
pip3 install pathlib
```
for linux:
```commandline
sudo apt-get install python3-tk
```