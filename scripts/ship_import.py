from collections import namedtuple
from operator import itemgetter
import math


# ship data: [{'N': '<N>', 'X': '<X>', 'DN': '<DN>', 'coordinates': [Point(y='<y>', z='<z>'), Point(y='<y>', x='<z>')]}]

def calculate_distance(x1, y1, x2, y2):
    distance = math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)
    return distance


def calculate_angle(pt_1, pt_2, pt_3):
    angle = math.acos(((pt_2[0] - pt_1[0]) * (pt_3[0] - pt_2[0]) + (pt_2[1] - pt_1[1]) * (pt_3[1] - pt_2[1]))
                      / (calculate_distance(x1=pt_1[0], y1=pt_1[1], x2=pt_2[0], y2=pt_2[1]) *
                         calculate_distance(x1=pt_2[0], y1=pt_2[1], x2=pt_3[0], y2=pt_3[1])))
    angle = math.degrees(angle)
    return angle


def import_ship(path):
    ship_data = []
    print('Reading file ...\n')
    with open(path, 'rt') as file:
        imported_file = file.readlines()
        i = 0
        while i < len(imported_file):
            try:
                frame = {}
                spl_line = imported_file[i].split()
                if spl_line[0] == '&N':
                    j = i
                    frame['N'] = float(spl_line[1])
                    j += 1
                    spl_line = imported_file[j].split()
                    if spl_line[0] == '&X':
                        frame['X'] = float(spl_line[1])
                        j += 1
                        spl_line = imported_file[j].split()
                        if spl_line[0] == '&DN':
                            frame['DN'] = float(spl_line[1])
                            j += 1
                            spl_line = imported_file[j].split()
                            frame['coordinates'] = []
                            while spl_line[0] == '&YZ':
                                coordinates = namedtuple('Point', 'y z')
                                point = coordinates(y=float(spl_line[1]), z=float(spl_line[2]))
                                frame['coordinates'].append(point)
                                j += 1
                                try:
                                    spl_line = imported_file[j].split()
                                except IndexError:
                                    break
                            frame['coordinates'].sort(key=lambda pt: pt.z)
                i = j
                try:
                    frame['coordinates'] = frame['coordinates']
                    ship_data.append(frame)
                except KeyError:
                    pass
            except IndexError:
                i += 1
    try:
        ship_data.sort(key=itemgetter('X'))
    except KeyError:
        print('List of frames is UNSORTED!')
    return ship_data


def import_diam_butt_coords(ship_data):
    diam_butt_coords = []
    for frame in ship_data:
        try:
            x = frame['X']
            for pt in frame['coordinates']:
                if pt.y == 0:
                    coordinates = namedtuple('Point', 'x z')
                    point = coordinates(x=x, z=pt.z)
                    diam_butt_coords.append(point)
        except KeyError:
            pass
    sorted_diam_butt_coords = sort_points(diam_butt_coords)
    return sorted_diam_butt_coords


def collect_point_distances(mixed_coords):
    points_distances = []
    points_distance = namedtuple('Distances', 'point_1 point_2 distance')
    try:
        for pt_1 in mixed_coords:
            for pt_2 in mixed_coords:
                dist = calculate_distance(x1=pt_1[0], y1=pt_1[1], x2=pt_2[0], y2=pt_2[1])
                data = points_distance(point_1=pt_1, point_2=pt_2, distance=dist)
                points_distances.append(data)
    except IndexError:
        pass
    return points_distances


def get_dist_btw_pts(points_distances, pt_1, pt_2):
    for line in points_distances:
        if line.point_1 == pt_1 and line.point_2 == pt_2:
            return line.distance


def sort_points(mixed_coords):
    points_distances = collect_point_distances(mixed_coords)
    sorted_coords = []
    start_point = mixed_coords[0]
    sorted_coords.append(start_point)
    second_point = None
    dist_to_start = 1000
    for pt in mixed_coords:
        dist = get_dist_btw_pts(points_distances, start_point, pt)
        if dist < dist_to_start and dist != 0:
            dist_to_start = dist
            second_point = pt
    sorted_coords.append(second_point)
    while len(sorted_coords) < len(mixed_coords):
        points_values = []
        for pt in mixed_coords:
            point_value = namedtuple('Angles', 'point angle distance value')
            if pt not in sorted_coords:
                angle = calculate_angle(pt_1=sorted_coords[-2], pt_2=sorted_coords[-1], pt_3=pt)
                dist_btw_pts = get_dist_btw_pts(points_distances, pt, sorted_coords[-1])
                value = dist_btw_pts + 0.021 * angle
                data = point_value(pt, angle, dist_btw_pts, value)
                points_values.append(data)
        next_point = None
        min_value = 1000
        for point in points_values:
            pt_value = point.value
            if pt_value < min_value:
                min_value = pt_value
                next_point = point.point
        sorted_coords.append(next_point)
    return sorted_coords
