from pathlib import Path


def switch_from_to(folder_to):
    folder_from = '/scripts'
    this_file_path = Path.cwd()
    str_path = str(this_file_path)
    go_back = str_path[:-len(folder_from)]
    go_further = go_back + folder_to
    return go_further
