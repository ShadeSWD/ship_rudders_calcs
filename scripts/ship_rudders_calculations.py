# Point(x=-70.898, z=8.369)

import E_functions

from collections import namedtuple

coordinates = namedtuple('Point', 'x z')
frm_tuple = namedtuple('Frame', 'x square b')


def interpolate(x1, y1, x2, y2, x3):
    y3 = y1 + ((x3 - x1) * (y2 - y1)) / (x2 - x1)
    return y3


def calculate_triangle_square(x1, x2, y1, y2, x3, y3):
    square = 0.5 * abs((x2 - x3) * (y1 - y3) - (x1 - x3) * (y2 - y3))
    return square


def find_neighbours(half_polygon_coords, z):
    i = 0
    max_coord = None
    min_coord = None
    while i < len (half_polygon_coords):
        if half_polygon_coords[i].z > z:
            i += 1
        else:
            max_coord = half_polygon_coords[i-1]
            min_coord = half_polygon_coords[i]
            break
    return min_coord, max_coord


def calculate_x_0(polygon_coords, z_max):
    cm = []
    left_coords = []
    right_coords = []
    for coord in polygon_coords:
        if coord.x <= 0:
            left_coords.append(coord)
        else:
            right_coords.append(coord)
    right_coords.reverse()
    height_bit = z_max / 1000
    lcm = namedtuple('LCM', 'x z')
    rcm = namedtuple('RCM', 'x z')
    curr_line = 0
    while curr_line < z_max:
        min_coord, max_coord = find_neighbours(half_polygon_coords=left_coords, z=curr_line)
        x_c = interpolate(x1=min_coord.z, y1=min_coord.x, x2=max_coord.z, y2=max_coord.x, x3=curr_line)
        cm.append(lcm(x=x_c, z=curr_line))

        min_coord, max_coord = find_neighbours(half_polygon_coords=right_coords, z=curr_line)
        x_c = interpolate(x1=min_coord.z, y1=min_coord.x, x2=max_coord.z, y2=max_coord.x, x3=curr_line)
        cm.append(rcm(x=x_c, z=curr_line))

        curr_line += height_bit
    sx = 0
    sum_length = 0
    for elem in cm:
        sx += abs(elem.x) * (elem.x / 2)
        sum_length += abs(elem.x)
    x_c = sx / sum_length
    return x_c


def calculate_polygon_square(polygon_coords):
    square = 0
    i = 0
    while i < len(polygon_coords):
        try:
            square += 0.5 * (polygon_coords[i][0] * polygon_coords[i + 1][1] -
                             polygon_coords[i][1] * polygon_coords[i + 1][0])
            i += 1
        except IndexError:
            square += 0.5 * (polygon_coords[i][0] * polygon_coords[0][1] - polygon_coords[i][1] * polygon_coords[0][0])
            i += 1
    square = abs(square)
    return square


def calculate_frame_squares(ship_data, level):
    frames_info = []
    for frame in ship_data:
        x = frame['X']
        b = 0
        frame_coords = get_frame_coords(ship_data=ship_data, frame_x=x, level=level)
        square = calculate_polygon_square(polygon_coords=frame_coords)
        for pt in frame_coords:
            if pt.y > b:
                b = pt.y
        frames_info.append(frm_tuple(x=x, square=square, b=b))
    return frames_info


def calculate_ship_volume(ship_data, level):
    frames_info = calculate_frame_squares(ship_data=ship_data, level=level)
    squares_to_volume = [coordinates(x=frames_info[0].x, z=0)]
    for frame in frames_info:
        squares_to_volume.append(coordinates(x=frame.x, z=frame.square))
    squares_to_volume.append(coordinates(x=frames_info[-1].x, z=0))
    half_volume = calculate_polygon_square(squares_to_volume)
    return half_volume


def calculate_breadth(ship_data, level):
    frames_info = calculate_frame_squares(ship_data=ship_data, level=level)
    b_max = 0
    for frame in frames_info:
        if frame.b > b_max:
            b_max = frame.b
    return b_max


def calculate_length(diam_butt_coords, level):
    first_pt = None
    first_pt_index = None
    last_pt = None
    last_pt_index = None
    i = 0
    while i < len(diam_butt_coords):
        if diam_butt_coords[i].z <= level:
            first_pt = diam_butt_coords[i]
            first_pt_index = i
            break
        i += 1
    i = first_pt_index + 1
    while i < len(diam_butt_coords):
        if diam_butt_coords[i].z >= level:
            last_pt = diam_butt_coords[i]
            last_pt_index = i
            break
        i += 1
    first_pt_x = interpolate(x1=first_pt.z, y1=first_pt.x, x2=diam_butt_coords[first_pt_index - 1].z,
                             y2=diam_butt_coords[first_pt_index - 1].x, x3=level)
    last_pt_x = interpolate(x1=last_pt.z, y1=last_pt.x, x2=diam_butt_coords[last_pt_index - 1].z,
                            y2=diam_butt_coords[last_pt_index - 1].x, x3=level)
    length = abs(first_pt_x - last_pt_x)
    return length


def get_frame_coords(ship_data, frame_x, level):
    frame_coords = []
    coords = namedtuple('Point', 'y z')
    extra_pt = None
    flag = False
    for frame in ship_data:
        try:
            if frame['X'] == frame_x:
                for pt in frame['coordinates']:
                    if pt.z <= level:
                        point = coords(y=pt.y, z=pt.z)
                        frame_coords.append(point)
                    elif not flag:
                        flag = True
                        extra_pt = coords(y=pt.y, z=pt.z)
        except KeyError:
            pass
    try:
        last_point_y = interpolate(x1=frame_coords[-1].z, y1=frame_coords[-1].y, x2=extra_pt.z, y2=extra_pt.y, x3=level)
        last_point = coords(y=last_point_y, z=level)
        if frame_coords[-1] != last_point:
            frame_coords.append(last_point)
        frame_coords.append(coords(y=0, z=level))
    except IndexError:
        pass
    return frame_coords


def calculate_f_0(propellers):
    f_0 = float(0)
    if propellers != 1:
        pass
    return f_0


def calculate_c_b(half_volume, draft, half_breadth, length):
    c_b = half_volume / (draft * half_breadth * length)
    return c_b


def calculate_c_p(c_m, c_b):
    c_p = c_b / c_m
    return c_p


def calculate_sigma_k(stern_valance_square, length, draft, f_0):
    sigma_k = 1 - 2 * (stern_valance_square - f_0) / (length * draft)
    return sigma_k


def calculate_e_1(c_p, sigma_k):
    e_1 = E_functions.interpolate_e(c=c_p, sigma=sigma_k)
    return e_1


def calculate_c_m(ship_data, level):
    middle_x = None
    delta_x = 1000
    for frame in ship_data:
        delta = abs(frame['X'])
        if delta < delta_x:
            middle_x = frame['X']
            delta_x = delta
    frame_coords = get_frame_coords(ship_data=ship_data, frame_x=middle_x, level=level)
    middle_frame_square = calculate_polygon_square(frame_coords)
    y_max = 0
    z_max = 0
    for pt in frame_coords:
        if pt.y > y_max:
            y_max = pt.y
        if pt.z > z_max:
            z_max = pt.z
    rectangle_square = y_max * z_max
    c_m = round(middle_frame_square/rectangle_square, 3)
    return c_m


def get_underwater_diam_butt_coords(diam_butt_coords, level):
    underwater_diam_butt_coords = []
    first_pt = None
    first_pt_index = None
    last_pt = None
    last_pt_index = None
    i = 0
    while i < len(diam_butt_coords):
        if diam_butt_coords[i].z <= level:
            first_pt = diam_butt_coords[i]
            first_pt_index = i
            break
        i += 1
    i = first_pt_index + 1
    while i < len(diam_butt_coords):
        if diam_butt_coords[i].z >= level:
            last_pt = diam_butt_coords[i]
            last_pt_index = i
            break
        i += 1

    first_pt_x = interpolate(x1=first_pt.z, y1=first_pt.x, x2=diam_butt_coords[first_pt_index - 1].z,
                             y2=diam_butt_coords[first_pt_index - 1].x, x3=level)
    last_pt_x = interpolate(x1=last_pt.z, y1=last_pt.x, x2=diam_butt_coords[last_pt_index - 1].z,
                            y2=diam_butt_coords[last_pt_index - 1].x, x3=level)
    first_pt_int = coordinates(x=first_pt_x, z=level)
    last_pt_int = coordinates(x=last_pt_x, z=level)
    underwater_diam_butt_coords.append(first_pt_int)
    underwater_diam_butt_coords.extend(diam_butt_coords[first_pt_index: last_pt_index])
    underwater_diam_butt_coords.append(last_pt_int)

    return underwater_diam_butt_coords


def get_stern_valance(diam_butt_coords, summer_draft):
    x_min = 0
    first_pt = None
    first_pt_index = None
    i = 0
    while i < len(diam_butt_coords):
        if diam_butt_coords[i].x < x_min and diam_butt_coords[i].z <= summer_draft:
            x_min = diam_butt_coords[i].x
            z_min = diam_butt_coords[i].z
            first_pt = diam_butt_coords[i]
            first_pt_index = i
            j = i + 1
            while diam_butt_coords[i].x == diam_butt_coords[j].x:
                try:
                    if diam_butt_coords[j].z < z_min:
                        first_pt = diam_butt_coords[i + 1]
                        first_pt_index = j
                        j += 1
                except IndexError:
                    pass
            i = j
        else:
            i += 1
    first_pt_x = interpolate(x1=first_pt.z, y1=first_pt.x, x2=diam_butt_coords[first_pt_index - 1].z,
                             y2=diam_butt_coords[first_pt_index - 1].x, x3=summer_draft)
    if first_pt_x != first_pt.x:
        first_pt = coordinates(x=first_pt_x, z=summer_draft)
    else:
        first_pt_index += 1

    last_pt = None
    last_pt_index = None
    i = first_pt_index
    while i < len(diam_butt_coords):
        if diam_butt_coords[i].z != 0:
            i += 1
        else:
            last_pt = diam_butt_coords[i]
            last_pt_index = i
            break

    extra_point = coordinates(x=first_pt.x, z=last_pt.z)

    stern_valance_coords = [first_pt]
    stern_valance_coords.extend(diam_butt_coords[first_pt_index: last_pt_index + 1])
    stern_valance_coords.append(extra_point)

    return stern_valance_coords


def calculate_lamda_p(h_p, a):
    lamda_p = (h_p ** 2) / a
    return lamda_p


def calculate_e_2(a_3, a_4, v, h_p, a, x_0, l_1):
    lamda_p = calculate_lamda_p(h_p, a)
    e_2 = ((3.8 * a_3) / ((v ** 2) * a_4)) * (1 - 0.0667 * a_3 / a_4) * (1 + (lamda_p - 1) * (0.33 + 0.015 * (v - 7.5))
                                                                         - 5 * x_0 / l_1)
    return e_2


def calculate_h_p(rudder_axe_x, diam_butt_coords):
    pt = None
    pt_index = None
    i = 0
    while i < len(diam_butt_coords):
        if diam_butt_coords[i].x >= rudder_axe_x:
            pt = diam_butt_coords[i]
            pt_index = i
            break
        i += 1
    h_p = interpolate(x1=pt.x, y1=pt.z, x2=diam_butt_coords[pt_index - 1].x, y2=diam_butt_coords[pt_index - 1].z,
                      x3=rudder_axe_x)
    return h_p
