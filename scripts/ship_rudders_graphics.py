from graphics import *

WINDOW_X = 1600
WINDOW_Y = 700
DB_SCALE = 10
FR_SCALE = 25
X_ZERO = WINDOW_X // 2
Y_ZERO = WINDOW_Y - 200


def draw_rudder_shaft(window, shaft_x, color, deck_z):
    try:
        shaft = Line(Point(shaft_x * DB_SCALE + X_ZERO, Y_ZERO), Point(shaft_x * DB_SCALE + X_ZERO,
                                                                       Y_ZERO - deck_z * DB_SCALE))
        shaft.setOutline(color)
        shaft.draw(window)
    except TypeError:
        pass


def draw_waterline(window, p_of_view, draft, color):
    try:
        if p_of_view == 'db':
            waterline = Line(Point(0, Y_ZERO - draft * DB_SCALE), Point(WINDOW_X, Y_ZERO - draft * DB_SCALE))
            waterline.setOutline(color)
            waterline.draw(window)
        elif p_of_view == 'fr':
            waterline = Line(Point(0, Y_ZERO - draft * FR_SCALE), Point(WINDOW_X, Y_ZERO - draft * FR_SCALE))
            waterline.setOutline(color)
            waterline.draw(window)
    except TypeError:
        pass


def create_graphics_window():
    print('Starting graphics window ...')
    window = GraphWin("ship", WINDOW_X, WINDOW_Y)
    window.setBackground("white")
    return window


def close_graphics_window(window):
    window.getMouse()
    window.close()
    print('Closing graphics window ...\n')


def draw_axes(window):
    print('Drawing axes...')
    axe_x = Line(Point(X_ZERO, 0), Point(X_ZERO, WINDOW_Y))
    axe_x.setOutline("grey")
    axe_x.draw(window)
    axe_y = Line(Point(0, Y_ZERO), Point(WINDOW_X, Y_ZERO))
    axe_y.setOutline("grey")
    axe_y.draw(window)


def draw_frames(ship_data, summer_draft):
    if not ship_data:
        raise ModuleNotFoundError
    window = create_graphics_window()
    draw_axes(window)
    draw_waterline(window=window, color="red", draft=summer_draft, p_of_view='fr')
    print('Drawing frames ...')
    for frame in ship_data:
        try:
            if frame['X'] < 0:
                direction = -1
            else:
                direction = 1
            prev_y = None
            prev_z = None
            for pt in frame['coordinates']:
                y = pt.y * FR_SCALE * direction + X_ZERO
                z = pt.z * -FR_SCALE + Y_ZERO
                obj = Circle(Point(y, z), 1)
                obj.draw(window)
                try:
                    line = Line(Point(y, z), Point(prev_y, prev_z))
                    line.setOutline("blue")
                    line.draw(window)
                except TypeError:
                    pass
                prev_y = y
                prev_z = z
        except IndexError:
            pass
        except KeyError:
            pass

    close_graphics_window(window)


def draw_diametrical_buttock(ship_data, diam_butt_coords, summer_draft, rudder_axe_x):
    if not ship_data:
        raise ModuleNotFoundError
    window = create_graphics_window()
    draw_axes(window)
    draw_waterline(window=window, color="red", draft=summer_draft, p_of_view='db')
    deck_z = ship_data[0]['coordinates'][-1].z
    draw_rudder_shaft(window=window, shaft_x=rudder_axe_x, color="green", deck_z=deck_z)

    print('Drawing diametrical buttock ...')
    for frame in ship_data:
        try:
            x = frame['X'] * DB_SCALE + X_ZERO
            for pt in frame['coordinates']:
                if pt.y == 0:
                    z = pt.z * -DB_SCALE + Y_ZERO
                    obj = Circle(Point(x, z), 1)
                    obj.setOutline("blue")
                    obj.draw(window)
        except IndexError:
            pass
        except KeyError:
            pass
    prev_x = None
    prev_z = None
    for pt in diam_butt_coords:
        try:
            x = pt.x * DB_SCALE + X_ZERO
            z = pt.z * -DB_SCALE + Y_ZERO
            try:
                line = Line(Point(x, z), Point(prev_x, prev_z))
                line.setOutline("blue")
                line.draw(window)
                # window.getMouse()
            except TypeError:
                pass
            prev_x = x
            prev_z = z
        except KeyError:
            pass
    pt = diam_butt_coords[0]
    x = pt.x * DB_SCALE + X_ZERO
    z = pt.z * -DB_SCALE + Y_ZERO
    line = Line(Point(x, z), Point(prev_x, prev_z))
    line.setOutline("blue")
    line.draw(window)
    close_graphics_window(window)
