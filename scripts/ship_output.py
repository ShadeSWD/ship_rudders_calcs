def diam_butt_s2g(ship_data, path):
    if path == '':
        raise FileNotFoundError
    if not ship_data:
        raise ModuleNotFoundError
    file = open(path, 'w')
    print(f'Opening {path}...')
    print('Dumping...')
    for frame in ship_data:
        try:
            ###############################
            # Uncomment function you need #
            ###############################

            # for .s2g format:

            # string = '&N ' + str(frame['N']) + '\n'
            # file.write(string)
            # string = '&X ' + str(frame['X']) + '\n'
            # file.write(string)
            # string = '&DN ' + str(frame['DN']) + '\n'
            # file.write(string)
            # for pt in frame['coordinates']:
            #     if pt.y == 0:
            #         string = '&YZ ' + str(pt.y) + ' ' + str(pt.z) + '\n'
            #         file.write(string)

            # coordinates only mode

            for pt in frame['coordinates']:
                if pt.y == 0:
                    string = str(frame['X']) + ' ' + str(pt.z) + '\n'
                    file.write(string)

        except KeyError:
            pass
    print('Process finished!\n')
